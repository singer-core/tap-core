#!/usr/bin/env python

__version__ = '0.0.3'

from .api import main


if __name__ == '__main__':
    main()
