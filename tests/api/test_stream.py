from typing import Any, Dict, List, Iterable
# NOTE: from pprint import pprint; pprint([json.loads(o) for o in captured.out.split('\n')[:-1]], width=152)
from pathlib import Path
from copy import copy, deepcopy
import json
from asyncio import Condition

from pytest import fixture, raises
from tap.api.stream import AbstractStream, Stream, SubStream, StreamPages
from tap.api.transform import format_response_replication, format_response_sub_stream
from tap.api.sync import Extractor
from .conftest import AppApi


@fixture
def locations() -> List[Dict[str, Any]]:

    return [
        {'type': 'STATE', 'value': {'currently_syncing': 'locations'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Everywhere', 'parent_id': None, 'updated_at': 1649839191},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '2', 'name': 'Nowhere', 'parent_id': '1', 'updated_at': 1649839217},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'locations'}}]


@fixture
def users() -> List[Dict[str, Any]]:

    return [
        {'type': 'STATE', 'value': {'bookmarks': {'users': {'offset': {'offset': 0}}}, 'currently_syncing': 'users'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'users',
         'type': 'SCHEMA'},
        {'stream': 'users', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Eddy', 'updated_at': 1649839191},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '2', 'name': 'Sabrina', 'updated_at': 1649839191},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '3', 'name': 'John Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 0}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 0}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'tags'}},
        {'key_properties': ['user_id'],
         'schema': {'additionalProperties': False,
                    'properties': {'name': {'type': ['null', 'string']},
                                   'tag_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']},
                                   'user_id': {'type': ['null', 'string']}},
                    'type': ['null', 'object']},
         'stream': 'tags',
         'type': 'SCHEMA'},
        {'stream': 'tags', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '1'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '2'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '3'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 0}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'tags'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 3}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'users'}},
        {'record': {'created_at': 1649839217, 'id': '4', 'name': 'Elisabeth Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '5', 'name': 'Mary Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 3}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 3}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'tags'}},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '4'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '5'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 3}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'tags'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 6}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 6}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 6}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'tags'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'tags': {}, 'users': {'offset': {'offset': 6}, 'updated_at': 1649839217, 'version': 1651214378321}},
                   'currently_syncing': 'tags'}},
        {'stream': 'users', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321}]


@fixture
def tags() -> List[Dict[str, Any]]:

    return [
        {'type': 'STATE', 'value': {'currently_syncing': 'tags'}},
        {'key_properties': ['user_id'],
         'schema': {'additionalProperties': False,
                    'properties': {'name': {'type': ['null', 'string']},
                                   'tag_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']},
                                   'user_id': {'type': ['null', 'string']}},
                    'type': ['null', 'object']},
         'stream': 'tags',
         'type': 'SCHEMA'},
        {'stream': 'tags', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '1'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '2'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '3'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '4'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '5'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'tags': {}}, 'currently_syncing': 'tags'}}]


async def test_stream_abstract_get_stream(capsys, patch_datetime, schemas_path: Path, config: Dict[str, Any]) -> None:

    streams: tuple = (
        # NOTE: Incremental Non-Paginated API Responses
        AbstractStream(
            'locations', config=config, schemas_dir=schemas_path, key_properties=['id'], replication_method='FULL_TABLE',
            format_response_function=format_response_replication,
            format_response_params={'path': 'locations'}),)

    with raises(NotImplementedError):
        await streams[0].get_stream(Extractor(config, streams, {}))


async def test_stream_write_schema(capsys, patch_datetime, schemas_path: Path, config: Dict[str, Any]) -> None:

    streams: set = {
        # NOTE: Incremental Non-Paginated API Responses
        copy(Stream(
            'locations', config=config, schemas_dir=schemas_path, key_properties=['id'], replication_method='FULL_TABLE',
            format_response_function=format_response_replication,
            format_response_params={'path': 'locations'}))
    }

    x: Extractor = Extractor(
        config,
        streams,
        {'bookmarks': {'locations': {'updated_at': 1649839191}}, 'currently_syncing': None})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].write_schema(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'stream': 'locations', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321}]


async def test_stream_write_records(capsys, patch_datetime, streams: Iterable, config: Dict[str, Any], app_api: AppApi) -> None:

    x: Extractor = Extractor(config, streams, {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[1]
        await x.streams[stream.tap_stream_id].write_records(x, app_api.users)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'currently_syncing': 'users'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'users',
         'type': 'SCHEMA'},
        {'stream': 'users', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Eddy', 'updated_at': 1649839191},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '2', 'name': 'Sabrina', 'updated_at': 1649839191},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '3', 'name': 'John Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '4', 'name': 'Elisabeth Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '5', 'name': 'Mary Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321}]


async def test_stream_get_stream(
    capsys, patch_datetime, patch_session: Any, streams: Iterable, schemas_path: Path, config: Dict[str, Any], locations: Any
) -> None:

    x: Extractor = Extractor(config, deepcopy(streams), {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == locations

    x: Extractor = Extractor(
        config,
        streams,
        {'bookmarks': {'locations': {'updated_at': 1649839191}}, 'currently_syncing': None})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {'updated_at': 1649839191}}, 'currently_syncing': 'locations'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'record': {'created_at': 1649839217, 'id': '2', 'name': 'Nowhere', 'parent_id': '1', 'updated_at': 1649839217},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'locations'}}]

    x: Extractor = Extractor(config, (
        # NOTE: Incremental Non-Paginated API Responses
        Stream(
            'locations', config=config, schemas_dir=schemas_path, key_properties=['id'], replication_key='updated_at',
            format_response_function=format_response_replication,
            format_response_params={'path': 'locations', 'replication_key': 'updated_at'}),

        # NOTE: Full Non-Paginated API Responses
        SubStream(
            'tags', config=config, schemas_dir=schemas_path, key_properties=['user_id'],
            parent={'tap_stream_id': 'locations', 'key_properties': ['id']},
            format_response_function=format_response_sub_stream,
            format_response_params={'path': 'tags'},
            params={'endpoint': 'users/{id}/tags', 'ignore_status': [404]})
    ), {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'currently_syncing': 'locations'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'stream': 'locations', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Everywhere', 'parent_id': None, 'updated_at': 1649839191},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '2', 'name': 'Nowhere', 'parent_id': '1', 'updated_at': 1649839217},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'stream': 'locations', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {}}, 'currently_syncing': 'locations'}},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {}}, 'currently_syncing': 'tags'}},
        {'key_properties': ['user_id'],
         'schema': {'additionalProperties': False,
                    'properties': {'name': {'type': ['null', 'string']},
                                   'tag_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']},
                                   'user_id': {'type': ['null', 'string']}},
                    'type': ['null', 'object']},
         'stream': 'tags',
         'type': 'SCHEMA'},
        {'stream': 'tags', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '1'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '2'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {}, 'tags': {}}, 'currently_syncing': 'tags'}}]

    x: Extractor = Extractor(config, {
        Stream(
            'locations', config=config, schemas_dir=schemas_path, key_properties=['id'],
            format_response_function=format_response_replication,
            format_response_params={'path': 'locations'})
    }, {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'currently_syncing': 'locations'}},
        {'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'stream': 'locations', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Everywhere', 'parent_id': None, 'updated_at': 1649839191},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '2', 'name': 'Nowhere', 'parent_id': '1', 'updated_at': 1649839217},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'stream': 'locations', 'type': 'ACTIVATE_VERSION', 'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {}}, 'currently_syncing': 'locations'}}]

    x: Extractor = Extractor(config, {
        # NOTE: Incremental Non-Paginated API Responses
        Stream(
            'locations', config=config, schemas_dir=schemas_path, key_properties=['id'],
            replication_method='INCREMENTAL', replication_key='updated_at',
            format_response_function=format_response_replication,
            format_response_params={'path': 'locations'})
    }, {'bookmarks': {'locations': {'updated_at': 1649839191}}})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {'updated_at': 1649839191}}, 'currently_syncing': 'locations'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'locations',
         'type': 'SCHEMA'},
        {'record': {'created_at': 1649839217, 'id': '2', 'name': 'Nowhere', 'parent_id': '1', 'updated_at': 1649839217},
         'stream': 'locations',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'locations': {'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'locations'}}]


async def test_stream_get_pages(capsys, patch_datetime, patch_session: Any, streams: Iterable, schemas_path: Path, config: Dict[str, Any], users: Any) -> None:

    x: Extractor = Extractor(config, streams, {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[1]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == users

    streams: List = (
        # NOTE: Full Paginated API Responses
        StreamPages(
            'users', config=config, schemas_dir=schemas_path, key_properties=['id'], replication_method='INCREMENTAL', replication_key='updated_at',
            format_response_function=format_response_replication,
            format_response_params={'path': 'users'},
            params={'offset_key': 'offset', 'limit_key': 'limit', 'params': {'limit': 3}}),
    )

    x: Extractor = Extractor(config, streams, {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'bookmarks': {'users': {'offset': {'offset': 0}}}, 'currently_syncing': 'users'}},
        {'bookmark_properties': ['updated_at'],
         'key_properties': ['id'],
         'schema': {'additionalProperties': False,
                    'properties': {'created_at': {'type': ['null', 'integer']},
                                   'id': {'type': ['null', 'string']},
                                   'name': {'type': ['null', 'string']},
                                   'parent_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']}},
                    'type': ['null', 'object']},
         'stream': 'users',
         'type': 'SCHEMA'},
        {'record': {'created_at': 1649839191, 'id': '1', 'name': 'Eddy', 'updated_at': 1649839191},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839191, 'id': '2', 'name': 'Sabrina', 'updated_at': 1649839191},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '3', 'name': 'John Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 0}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 3}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'users'}},
        {'record': {'created_at': 1649839217, 'id': '4', 'name': 'Elisabeth Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'created_at': 1649839217, 'id': '5', 'name': 'Mary Doe', 'updated_at': 1649839217},
         'stream': 'users',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 3}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 6}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'users'}},
        {'type': 'STATE',
         'value': {'bookmarks': {'users': {'offset': {'offset': 6}, 'updated_at': 1649839217, 'version': 1651214378321}}, 'currently_syncing': 'users'}}]


async def test_stream_get_stream_child(
        capsys, patch_datetime, streams, patch_session: Any, config: Dict[str, Any], schemas_path: Path, app_api: AppApi, tags: Any) -> None:

    x: Extractor = Extractor(config, streams, {})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[2]
        await x.streams[stream.tap_stream_id].get_stream(x, app_api.users)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == tags

    x: Extractor = Extractor(config, (
        SubStream(
            'tags', config=config, schemas_dir=schemas_path, key_properties=['user_id'],
            replication_method='INCREMENTAL', replication_key='user_id',
            parent={'tap_stream_id': 'users', 'key_properties': ['id']},
            format_response_function=format_response_sub_stream,
            format_response_params={'path': 'tags'},
            params={'endpoint': 'users/{id}/tags', 'ignore_status': [404]}),), {'bookmarks': {'tags': {'user_id': 3}}})

    async with x.client(x.config) as x._client:
        x._condition: Condition = Condition()
        x._currently_syncing: set = set()
        stream = x.catalog.streams[0]
        await x.streams[stream.tap_stream_id].get_stream(x, app_api.users)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [
        {'type': 'STATE', 'value': {'bookmarks': {'tags': {'user_id': 3}}, 'currently_syncing': 'tags'}},
        {'bookmark_properties': ['user_id'],
         'key_properties': ['user_id'],
         'schema': {'additionalProperties': False,
                    'properties': {'name': {'type': ['null', 'string']},
                                   'tag_id': {'type': ['null', 'string']},
                                   'updated_at': {'type': ['null', 'integer']},
                                   'user_id': {'type': ['null', 'string']}},
                    'type': ['null', 'object']},
         'stream': 'tags',
         'type': 'SCHEMA'},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '4'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'record': {'name': 'Glorious', 'tag_id': '1', 'user_id': '5'},
         'stream': 'tags',
         'time_extracted': '2022-04-29T06:39:38.321056Z',
         'type': 'RECORD',
         'version': 1651214378321},
        {'type': 'STATE', 'value': {'bookmarks': {'tags': {}}, 'currently_syncing': 'tags'}}]
