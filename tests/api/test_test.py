from typing import Any, Dict, List
from pathlib import Path

from aiohttp import ClientSession, TCPConnector, web
from aiohttp.web import Application, Response, json_response
from aiohttp.test_utils import TestServer

from pytest import fixture

from tap.api.test import AppResolver, AppServer, AppClient
from .conftest import HOST, TOKEN


class SimpleApp:

    def __init__(self) -> None:
        self.app = Application()
        self.app.router.add_routes(
            [
                web.get('/v3.9/me', self.get_me),
                web.get('/v3.9/me/friends', self.get_my_friends),
            ]
        )

        self.app.router.add_route('*', '/v3.9/', self.request_root)

        self.root_str = 'Hello, world'
        self.root_bytes = self.root_str.encode('utf-8')
        self.me = {"name": "John Doe", "id": "12345678901234567"}
        self.my_friends = {
            "data": [
                {"name": "Bill Doe", "id": "233242342342"},
                {"name": "Mary Doe", "id": "2342342343222"},
                {"name": "Alex Smith", "id": "234234234344"},
            ],
            "paging": {
                "cursors": {
                    "before": "QVFIUjRtc2c5NEl0ajN",
                    "after": "QVFIUlpFQWM0TmVuaDRad0dt",
                },
                "next": (
                    f"https://{HOST}/v3.9/12345678901234567/"
                    "friends?access_token=EAACEdEose0cB"
                ),
            },
            "summary": {"total_count": 3},
        }

    async def get_me(self, request: web.Request) -> web.StreamResponse:
        return json_response(self.me)

    async def get_my_friends(self, request: web.Request) -> web.StreamResponse:
        return json_response(self.my_friends)

    async def request_root(self, request: web.Request) -> Response:
        return Response(body=self.root_bytes)


@fixture
def app() -> Application:
    return SimpleApp().app


@fixture
async def server(app: Application) -> TestServer:
    async with TestServer(app, host='localhost', skip_url_asserts=True) as server:
        yield server


@fixture
async def connector(server: TestServer, host: str) -> TCPConnector:
    return TCPConnector(resolver=AppResolver({host: server.port}), ssl=False)


@fixture
async def session(server: TestServer, connector: TCPConnector) -> AppClient:
    async with AppClient(server, connector=connector) as client:
        yield client


# @fixture
# async def app_server(app_api: AppApi, ssl_cert: Path, ssl_key: Path) -> AppServer:
#     async with AppServer(app_api.app, str(ssl_cert), str(ssl_key)) as server:
#         yield server


# @fixture
# async def client_session(host: str, app_server: Path) -> AppServer:
#     async with ClientSession(connector=TCPConnector(resolver=AppResolver({host: app_server.port}), ssl=False)) as client:
#         yield client


@fixture
async def client_session(connector: TCPConnector) -> ClientSession:
    async with ClientSession(connector=connector) as client:
        yield client


async def test_resolver(host: str) -> None:
    resolver: AppResolver = AppResolver({host: None})
    result: List[Dict[str, Any]] = await resolver.resolve(host)
    assert result[0]['port'] == 0

    closure = await resolver.close()
    assert closure is None


async def test_session_https_get(host: str, ssl_cert: Path, ssl_key: Path) -> None:
    async with AppServer(SimpleApp().app, str(ssl_cert), str(ssl_key)) as server:
        resolver: AppResolver = AppResolver({host: server.port})
        connector: TCPConnector = TCPConnector(resolver=resolver, ssl=False)
        async with ClientSession(connector=connector) as client:

            async with client.get(
                f'https://{host}/v3.9/'
            ) as resp:
                r = await resp.text()
                assert r == 'Hello, world'


async def test_session_request(session: AppClient) -> None:
    async with session.request('GET', '/v3.9/') as resp:
        assert resp.status == 200
        text = await resp.text()
        assert text == 'Hello, world'


async def test_session_head(host: str, client_session: ClientSession) -> None:
    resp = await client_session.head(f'http://{host}/v3.9/')
    assert resp.status == 200


async def test_session_get(host: str, client_session: ClientSession) -> None:
    async with client_session.get(
        f'http://{host}/v3.9/me', params={'access_token': TOKEN}
    ) as resp:
        r = await resp.json()
        assert r == SimpleApp().me


async def test_main(host: str, ssl_cert: Path, ssl_key: Path) -> None:

    async with AppServer(SimpleApp().app, str(ssl_cert), str(ssl_key)) as server:
        resolver = AppResolver({host: server.port})
        connector = TCPConnector(resolver=resolver, ssl=False)

        async with ClientSession(connector=connector) as session:
            async with session.get(
                f'https://{host}/v3.9/me', params={'access_token': TOKEN}
            ) as resp:
                r = await resp.json()
                assert r == SimpleApp().me

            async with session.get(
                f'https://{host}/v3.9/me/friends', params={'access_token': TOKEN}
            ) as resp:
                r = await resp.json()
                assert r == SimpleApp().my_friends
