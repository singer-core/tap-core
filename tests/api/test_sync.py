from typing import Any, Dict, Iterable
# NOTE: from pprint import pprint; pprint([json.loads(o) for o in captured.out.split('\n')[:-1]], width=152)
from pathlib import Path
import json
from singer import utils

from pytest import raises  # , mark
from tap import api
from tap.api.sync import Extractor, set_streams, sync


async def test_extractor_get_streams(capsys, patch_datetime, patch_session: Any, streams: Iterable, config: Dict[str, Any], full_sync: Any) -> None:

    await Extractor(config, streams).get_streams()

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == full_sync


def test_extractor_run(capsys, config: Dict[str, Any]) -> None:

    Extractor(config, [], {}).run()

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [{'type': 'STATE', 'value': {'currently_syncing': None}}]


def test_set_streams():

    assert set_streams() == []


async def test_sync(
    monkeypatch, capsys, patch_datetime, patch_argument_parser, patch_set_streams,
    patch_session: Any, schemas_path: Path,
    config: Dict[str, Any], full_sync: Any
) -> None:

    await sync(schemas_path, api.set_streams)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == full_sync

    class ArgumentParser:

        def __init__(self, x):
            self.config = config
            self.state = {}
            self.catalog = None
            self.discover = True

        def add_argument(self, x, y, help='Dummy config file', required=False):
            pass

        def parse_args(self):
            return self

    monkeypatch.setattr(utils, 'parse_args', ArgumentParser)

    captured = capsys.readouterr()
    assert captured.out == ''

    with raises(Exception):
        await sync(Path(__file__).parent, print)
