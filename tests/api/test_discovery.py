from typing import Iterable
from singer import Catalog
from pytest import fixture

from tap.api.discovery import discover


@fixture
def catalog() -> None:

    return Catalog.from_dict(
        {'streams': [
            {'is_view': False,
             'key_properties': ['id'],
             'metadata': [{'breadcrumb': (),
                           'metadata': {'forced-replication-method': 'INCREMENTAL',
                                        'inclusion': 'available',
                                        'schema-name': 'locations',
                                        'selected': True,
                                        'table-key-properties': ['id'],
                                        'valid-replication-keys': 'updated_at'}},
                          {'breadcrumb': ('properties', 'id'), 'metadata': {'inclusion': 'automatic'}},
                          {'breadcrumb': ('properties', 'name'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'parent_id'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'created_at'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'updated_at'), 'metadata': {'inclusion': 'available'}}],
             'replication_key': 'updated_at',
             'replication_method': 'INCREMENTAL',
             'schema': {'additionalProperties': False,
                        'properties': {'created_at': {'type': ['null', 'integer']},
                                       'id': {'type': ['null', 'string']},
                                       'name': {'type': ['null', 'string']},
                                       'parent_id': {'type': ['null', 'string']},
                                       'updated_at': {'type': ['null', 'integer']}},
                        'type': ['null', 'object']},
             'stream': 'locations',
             'stream_alias': 'locations',
             'tap_stream_id': 'locations'},
            {'is_view': False,
             'key_properties': ['id'],
             'metadata': [{'breadcrumb': (),
                           'metadata': {'forced-replication-method': 'FULL_TABLE',
                                        'inclusion': 'available',
                                        'schema-name': 'users',
                                        'selected': True,
                                        'table-key-properties': ['id'],
                                        'valid-replication-keys': 'updated_at'}},
                          {'breadcrumb': ('properties', 'id'), 'metadata': {'inclusion': 'automatic'}},
                          {'breadcrumb': ('properties', 'name'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'parent_id'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'created_at'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'updated_at'), 'metadata': {'inclusion': 'available'}}],
             'replication_key': 'updated_at',
             'replication_method': 'FULL_TABLE',
             'schema': {'additionalProperties': False,
                        'properties': {'created_at': {'type': ['null', 'integer']},
                                       'id': {'type': ['null', 'string']},
                                       'name': {'type': ['null', 'string']},
                                       'parent_id': {'type': ['null', 'string']},
                                       'updated_at': {'type': ['null', 'integer']}},
                        'type': ['null', 'object']},
             'stream': 'users',
             'stream_alias': 'users',
             'tap_stream_id': 'users'},
            {'is_view': False,
             'key_properties': ['user_id'],
             'metadata': [{'breadcrumb': (),
                           'metadata': {'forced-replication-method': 'FULL_TABLE',
                                        'inclusion': 'available',
                                        'schema-name': 'tags',
                                        'selected': True,
                                        'table-key-properties': ['user_id']}},
                          {'breadcrumb': ('properties', 'user_id'), 'metadata': {'inclusion': 'automatic'}},
                          {'breadcrumb': ('properties', 'tag_id'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'name'), 'metadata': {'inclusion': 'available'}},
                          {'breadcrumb': ('properties', 'updated_at'), 'metadata': {'inclusion': 'available'}}],
             'replication_method': 'FULL_TABLE',
             'schema': {'additionalProperties': False,
                        'properties': {'updated_at': {'type': ['null', 'integer']},
                                       'name': {'type': ['null', 'string']},
                                       'tag_id': {'type': ['null', 'string']},
                                       'user_id': {'type': ['null', 'string']}},
                        'type': ['null', 'object']},
             'stream': 'tags',
             'stream_alias': 'tags',
             'tap_stream_id': 'tags'}]})


def test_discover(streams: Iterable, catalog: Catalog) -> None:

    assert discover(streams) == catalog
