from tap.api.transform import format_response, format_response_replication, format_response_sub_stream
from .conftest import AppApi


async def test_format_response(app_api: AppApi) -> None:
    assert format_response((app_api.locations,)) == [app_api.locations]


async def test_format_response_replication(app_api: AppApi) -> None:
    assert format_response_replication((app_api.locations,), {'path': 'locations'}) == list(app_api.locations['locations'].values())
    assert format_response_replication(
        (app_api.locations,),
        {'path': 'locations', 'replication_key': 'updated_at'},
        1649839191) == list(app_api.locations['locations'].values())


async def test_format_response_sub_stream(app_api: AppApi) -> None:
    assert format_response_sub_stream((app_api.tags_1,), {'path': 'tags'}) == [{'user_id': '1', 'tag_id': '1', 'name': 'Glorious'}]
    assert format_response_sub_stream(
        (app_api.tags_1,),
        {'path': 'tags', 'replication_key': 'created_at'},
        {'created_at': 1653567457},
        0) == [{'user_id': '1', 'tag_id': '1', 'name': 'Glorious'}]
    assert format_response_sub_stream(
        (app_api.tags_1,),
        {'path': 'tags', 'replication_key': 'created_at'},
        {'created_at': 1653567457},
        1653567457) == [{'user_id': '1', 'tag_id': '1', 'name': 'Glorious'}]
