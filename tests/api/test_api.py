from typing import Any, Dict
# NOTE: from pprint import pprint; pprint([json.loads(o) for o in captured.out.split('\n')[:-1]], width=152)
from pathlib import Path
import json

from tap.api import main


def test_main(capsys, patch_datetime, patch_argument_parser, patch_set_streams, patch_session: Any, config: Dict[str, Any], schemas_path: Path) -> None:

    main(schemas_path)

    captured = capsys.readouterr()
    assert [json.loads(o) for o in captured.out.split('\n')[:-1]] == [{'type': 'STATE', 'value': {'currently_syncing': None}}]
