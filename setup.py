#!/usr/bin/env python

from setuptools import setup

setup(
    install_requires=[
        'singer-python~=5.0',
        'aiohttp~=3.0'
    ]
)
