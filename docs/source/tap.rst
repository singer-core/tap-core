Tap Reference
==================

.. note::
   This project is under active development.

Module contents
---------------

.. automodule:: tap
   :members:
   :undoc-members:
   :show-inheritance:

tap.api module
--------------

.. automodule:: tap.api
   :members:
   :undoc-members:
   :show-inheritance:

tap.api.client module
---------------------

.. automodule:: tap.api.client
   :members:
   :undoc-members:
   :show-inheritance:
