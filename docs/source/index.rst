.. Tap Core Documentation main file, created by
   sphinx-quickstart on Sat Apr 30 12:10:06 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Tap Core's Documentation!
=========================================

`Singer <https://www.singer.io/>`_ **tap-core** provide safe tools to easily extract
`Core API` data following
the `Singer spec <https://github.com/singer-io/getting-started/blob/master/docs/SPEC.md>`_ *convention* and *protocol*.

.. note::
   This project is under active development.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   modules

Submodules
----------

.. autosummary::
   :toctree: generated

..   tap.api
..   tap.api.client

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
