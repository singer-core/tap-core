# Change Log

## Unreleased
---

### New

### Changes

### Fixes

### Breaks

## [0.0.2](https://gitlab.com/singer-core/tap-core/tags/0.0.2) (2022-07-14)
---

### New
* #2 Improve Concurency & ACTIVATE_VERSION Singer extension support release by @omegax in !4

### Changes

### Fixes

### Breaks

**Full Changelog**: [`0.0.1...0.0.2`](https://gitlab.com/singer-core/tap-core/compare/0.0.1...0.0.2)

## [0.0.1](https://gitlab.com/singer-core/tap-core/tags/0.0.1) (2022-07-07)
---

### New

### Changes

### Fixes
* #1 Genesis: fix pypi release by @omegax in !2 !3

### Breaks

**Full Changelog**: [`0.0.0...0.0.1`](https://gitlab.com/singer-core/tap-core/compare/0.0.0...0.0.1)

## [0.0.0](https://gitlab.com/singer-core/tap-core/tags/0.0.0) (2022-06-19)
---

### New
* #1 Genesis by @omegax in !1

### Changes

### Fixes

### Breaks

**Full Changelog**: [`0...0.0.0`](https://gitlab.com/singer-core/tap-core/compare/0...0.0.0)
